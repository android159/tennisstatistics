package com.example.playtennis.Sign;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.playtennis.Main.MainActivity;

import com.example.playtennis.R;

public class SignActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText signInPassword, signInLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        Button signIn = (Button) findViewById(R.id.sign_in);
        Button registration = (Button) findViewById(R.id.registration);
        signInPassword = (EditText) findViewById(R.id.sign_in_password);
        signInLogin = (EditText) findViewById(R.id.sign_in_login);

        signIn.setOnClickListener(this);
        registration.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in:
                String login = signInLogin.getText().toString();
                String password = signInPassword.getText().toString();
                //TODO send data in bd and check
                if (!login.isEmpty() || !password.isEmpty()) {
                    Toast.makeText(this, "Заполнили", Toast.LENGTH_LONG).show();
//                    new ConnectionServer(this, login, password).execute();
                    Intent intent = new Intent(SignActivity.this, MainActivity.class);
                    startActivity(intent);
                    //TODO return true if exists, else false
                } else {
                    Toast.makeText(this, "Заполните поле логин или пароль", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.registration:
                // регистрация
                //TODO registration user
                Toast.makeText(this, "Регистрация", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
