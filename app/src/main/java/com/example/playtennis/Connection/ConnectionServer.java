package com.example.playtennis.Connection;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

//TODO study AsyncTask or SwingWorker
public class ConnectionServer extends AsyncTask<String, Void, String> {

    private Context context;
//    private static final String link = "https://androidtennis.000webhostapp.com/test.php:21";
    private String login, password;

    public ConnectionServer(Context context, String login, String password) {
        this.context = context;
        this.login = login;
        this.password = password;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    @Override
    protected String doInBackground(String... strings) {
        //TODO link with server for get data bd
        try {
            String link = "https://androidtennis.000webhostapp.com/test.php";
//            String data = URLEncoder.encode("login", "UTF-8") +  "=" + login + URLEncoder.encode("password", "UTF-8") +  "=" + password;

            URL url = new URL(link);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());

//            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;

            //Read response server
            while((line = reader.readLine()) != null) {
                sb.append(line);
                break;
            }

            return sb.toString();
        } catch (Exception e) {
            return new String("Exteption " + e.getMessage());
        }
    }
}
